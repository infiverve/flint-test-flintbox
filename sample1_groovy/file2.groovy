log.info('In file no ---2 ' + input.toString())
message1 = input.get('number1')
number2 = 'two'
con = message1 + number2
log.info("value of concat in file 2 ${con}")
response = call.bit('flint-test-flintbox:sample1_groovy:file3.groovy')
                .set('number2', con)
                .sync()
log.info('Response 2:  ' + response.toString())
output.set('response', response.get('response'))
log.info("output in file 2  ${output.raw()}")
log.info("exit code in file 2 ${response.exitcode()}")
log.info("response message in file 2 ${response.message()}")
