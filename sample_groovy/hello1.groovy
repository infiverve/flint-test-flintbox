log.info('Hello World  -- 1')
message = 'Welcome to flint!!!'

response = call.bit('flint-test-flintbox:sample_groovy:hello2.groovy')
                .set('flint_message', message)
                .sync()
log.info('Response 1:  ' + response.toString())
output.set('response', response.get('response'))
log.info("output in hello 1 ${output.raw()}")
