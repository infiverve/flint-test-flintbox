log.info('In flintbit 1 (sync)....')
message = 'Welcome to Flint!!!'
response = call.bit('flint-test-flintbox:sample5_groovy:fb2.groovy')
                .set('flint_message', message)
                .sync()

log.info(" output in 1 flintbit job id ${response.jobid()}")
output.set('response', response.jobid())
output.set('response', response.get('response'))
log.info('Response 1:  ' + response.toString())
