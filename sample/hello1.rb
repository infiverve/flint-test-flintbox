@log.info('Hello World  -- 1')
message = 'Welcome to flint!!!'
# @output.set("flint_message",message)
response = @call.bit('flint-test-flintbox:sample:hello2.rb')
                .set('flint_message', message)
                .sync
@log.info('Response 1:  ' + response.to_s)
@output.set('response', response.get('response'))
@log.info("output in hello 1 #{@output.raw}")
