log.trace("Started executing 'head' flintbit...")
//Flintbit Input Parameters
//mandatory
connector_name=input.get("connector_name")    //Name of the HTTP Connector
request_method=input.get("method")            //HTTP Request Method
request_url=input.get("url")                  //HTTP Request URL
//optional
request_timeout=input.get("timeout")          //HTTP Request Timeout in milliseconds, taken
                                               //by the Connector to serve the request
request_headers=input.get("headers")          //HTTP Request Headers
log.info('Flintbit input parameters are,')
log.info('connector name :: ' + connector_name.toString())
log.info('url :: ' + request_url.toString())
log.info('method :: ' + request_method.toString())
log.trace("Calling HTTP Connector...")
call_connector=call.connector(connector_name)
                    .set("method",request_method)
                    .set("url",request_url)
if(request_timeout==null)

    response=call_connector.sync()
else

  response=call_connector.set("timeout",request_timeout).sync()
  //HTTP Connector Response Meta Parameters
    response_exitcode=response.exitcode()           //Exit status code
    response_message=response.message()             //Execution status message
  //HTTP Connector Response Parameters

    response_headers=response.get("headers")      //Response Headers

  if (response.exitcode() == 0)
  {
  log.info('Success in executing HTTP Connector where, exitcode :: ' + response_exitcode)
  log.info(' message :: ' + response_message)

  log.info('HTTP Response Headers :: ' + response_headers)


  output.set("result",response_headers)
  log.trace("Finished executing 'head' flintbit with success...")
  }
  else
  {
  log.error('Failure in executing HTTP Connector where, exitcode :: ' + response_exitcode)
  log.info(' message :: ' + response_message)

  output.set("error",response_message)

  log.trace("Finished executing 'head' flintbit with error...")
  }
