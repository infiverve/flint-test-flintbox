@log.info('In demo no ---2 ' + @input.to_s)
exa1 = @input.get('exa')
@log.info(" value of exa in demo 2 #{exa1}")
@output.set('aa', exa1)
response = @call.bit('flint-test-flintbox:sample2:demo3.rb')
                .set('exa11', exa1)
                .sync
@log.info('value of response in demo 2  :  ' + response.to_s)
@output.set('response', response.get('response'))
