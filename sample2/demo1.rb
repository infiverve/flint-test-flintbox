@log.info('In demo no -- 1')
@log.info("Input in demo 1 #{@input.raw}")
response = @call.bit('flint-test-flintbox:sample2:demo2.rb')
                .set('exa', @input.to_s)
                .sync
@output.set('response', response.get('response'))
@log.info("final output in demo 1 #{@output.raw}")
