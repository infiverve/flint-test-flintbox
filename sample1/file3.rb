@log.info('In file no ---3 ' + @input.to_s)
message2 = @input.get('number2')
number3 = 'three'
@log.info("value of number 2 in file 3 #{@input.get('number2')}")
concatenate = message2 + number3
@log.info("value of concatenate in file 3 #{concatenate}")
response = @call.bit('flint-test-flintbox:sample1:file4.rb')
                .set('number3', concatenate)
                .sync
@log.info('Response 3:  ' + response.to_s)
@output.set('response', response.get('response'))
@log.info("output in file 3 #{@output.raw}")

@log.info("exit code in file 3 #{response.exitcode}")
@log.info("response message in file 3 #{response.message}")
