@log.info('In file no ---4 ' + @input.to_s)
message3 = @input.get('number3')
@log.info("value of number 3 in file 4 #{@input.get('number3')}")
number4 = 'four'
concatenate1 = message3 + number4
response = @call.bit('flint-test-flintbox:sample1:file5.rb')
                .set('number4', concatenate1)
                .sync
@log.info('Response 4:  ' + response.to_s)
@output.set('response', response.get('result'))
@log.info("output in file 4 #{@output.raw}")
